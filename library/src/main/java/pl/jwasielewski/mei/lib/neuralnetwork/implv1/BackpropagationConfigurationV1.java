package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.BackpropagationConfiguration;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class BackpropagationConfigurationV1 implements BackpropagationConfiguration {

    private double errorThreshold;
    private double momentum;
    private double learningRate;

    private List<String> trainingFiles;

    public BackpropagationConfigurationV1() {
        this(0.0, 0.0, 0.0);
    }

    public BackpropagationConfigurationV1(double errorThreshold, double momentum, double learningRate) {
        this.errorThreshold = errorThreshold;
        this.momentum = momentum;
        this.learningRate = learningRate;
    }

    @Override
    public double getErrorThreshold() {
        return this.errorThreshold;
    }

    @Override
    public void setErrorThreshold(double value) {
        this.errorThreshold = value;
    }

    @Override
    public double getMomentum() {
        return this.momentum;
    }

    @Override
    public void setMomentum(double value) {
        this.momentum = value;
    }

    @Override
    public void setLearingRate(double value) {
        this.learningRate = value;
    }

    @Override
    public double getLearningRate() {
        return this.learningRate;
    }

    @Override
    public void setTrainingFiles(List<String> files) {
        this.trainingFiles = files;
    }

    @Override
    public List<String> getAllTrainingDataFiles() {
        return this.trainingFiles;
    }

}
