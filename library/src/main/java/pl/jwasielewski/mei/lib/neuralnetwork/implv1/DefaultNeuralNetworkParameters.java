package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.NeuralNetworkCreatorParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class DefaultNeuralNetworkParameters implements NeuralNetworkCreatorParameters {

    private final int NUMBER_OF_LAYERS = 3;
    private final List<Integer> numberOfNeuronsInLayer;

    public DefaultNeuralNetworkParameters() {
        numberOfNeuronsInLayer = new ArrayList<>();
        numberOfNeuronsInLayer.add(576);    // input
        numberOfNeuronsInLayer.add(96);     // hidden
        numberOfNeuronsInLayer.add(16);     // output
    }

    @Override
    public void setNubmerOfLayers(int numberOfNeurons) {

    }

    @Override
    public void setNumberOfNeuronsInLayer(int layer, int numberOfNeurons) {

    }

    @Override
    public int getNumberOfLayers() {
        return NUMBER_OF_LAYERS;
    }

    @Override
    public int getNumberOfNeuronsInLayer(int layer) {
        return numberOfNeuronsInLayer.get(layer);
    }
}
