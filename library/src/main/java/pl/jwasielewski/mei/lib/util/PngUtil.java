package pl.jwasielewski.mei.lib.util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class PngUtil {

    private static final int SCALED_DIMENSION_PX = 24;

    private PngUtil() {

    }

    public static Image openPng(String path) throws IOException {
        return new Image(new File(path).toURI().toString());
    }

    public static void savePng(String path, Image image) throws IOException {
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", new File(path));
    }

    public static Image scaleImage(Image image, final int SRC_WIDTH, final int SRC_HEIGHT) {
        PixelReader pixelReader = image.getPixelReader();

        int top, right, bottom, left;
        top = SRC_HEIGHT;
        right = 0;
        left = SRC_WIDTH;
        bottom = 0;

        for (int y = 0; y < SRC_HEIGHT; ++y) {
            for (int x = 0; x < SRC_WIDTH; ++x) {
                if (!pixelReader.getColor(x, y).equals(Color.WHITE)) {
                    if (y < top) {
                        top = y;
                    }

                    if (y > bottom) {
                        bottom = y;
                    }

                    if (x > right) {
                        right = x;
                    }

                    if (x < left) {
                        left = x;
                    }
                }
            }
        }

//        System.out.println(String.format("[DEBUG] SRC_WIDTH:%d, SRC_HEIGHT:%d", SRC_WIDTH, SRC_HEIGHT));
//        System.out.println(String.format("[DEBUG] top:%d, right:%d, bottom:%d, left:%d", top, right, bottom, left));

//        if (DEBUG) {
//            gc.setStroke(Color.RED);
//            gc.strokeLine(left, 0, left, canvas.getHeight());
//            gc.setStroke(Color.GREEN);
//            gc.strokeLine(right, 0, right, canvas.getHeight());
//            gc.setStroke(Color.YELLOW);
//            gc.strokeLine(0, top, canvas.getWidth(), top);
//            gc.setStroke(Color.ORANGE);
//            gc.strokeLine(0, bottom, canvas.getWidth(), bottom);
//        }

        int width = right - left;
        int height = bottom - top;
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();

        WritablePixelFormat<IntBuffer> format = WritablePixelFormat.getIntArgbInstance();
        int[] buffer = new int[width * height];
        pixelReader.getPixels(left, top, width, height, format, buffer, 0, width);
        pixelWriter.setPixels(0, 0, width, height, format, buffer, 0, width);

        ByteArrayInOutStream byteOutput = new ByteArrayInOutStream();
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", byteOutput);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Image image1 = new Image(byteOutput.getInputStream());
        double scaledWidth = SCALED_DIMENSION_PX;
        double scaledHeight = SCALED_DIMENSION_PX;

        ImageView imageView = new ImageView(image1);
        imageView.setFitWidth(scaledWidth);
        imageView.setFitHeight(scaledHeight);
        imageView.setSmooth(true);
        Pane pane = new Pane(imageView);
        Scene offScreenScene = new Scene(pane);

        return imageView.snapshot(null, null);
    }
}
