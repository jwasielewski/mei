package pl.jwasielewski.mei.lib.neuralnetwork;

import pl.jwasielewski.mei.lib.util.ChineseCharacter;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public interface NeuralNetworkConfiguration {

    void addNewChineseCharacter(ChineseCharacter character);

    ChineseCharacter getCharacterForId(int id);

}
