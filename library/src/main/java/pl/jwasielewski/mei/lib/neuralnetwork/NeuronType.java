package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 14/11/2015
 */

public enum NeuronType {
    INPUT, HIDDEN, OUTPUT, BIAS
}
