package pl.jwasielewski.mei.lib.neuralnetwork.util;

import java.util.Random;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class WageRandomizer {

    private static WageRandomizer instance;

    public static WageRandomizer newInstance(int numberOfInputs) {
        return (instance = new WageRandomizer(numberOfInputs));
    }

    public static WageRandomizer getInstance() throws IllegalStateException {
        if (instance == null) {
            throw new IllegalStateException("Use newInstance before!");
        }
        return instance;
    }

    private Random generator;

    private final double MIN;
    private final double MAX;

    private WageRandomizer(int numberOfInputs) {
        generator = new Random(System.currentTimeMillis());

        MIN = -1 / Math.sqrt(numberOfInputs);
        MAX = 1 / Math.sqrt(numberOfInputs);
    }

    public double getRandomWeight() {
        return generator.nextDouble() * (MAX - MIN) + MIN;
    }

}
