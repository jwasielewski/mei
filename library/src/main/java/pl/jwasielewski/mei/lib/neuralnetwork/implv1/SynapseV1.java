package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.Neuron;
import pl.jwasielewski.mei.lib.neuralnetwork.Synapse;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class SynapseV1 implements Synapse {

    private double weight;
    private Neuron neuron;

    public SynapseV1() {
        this(null, 1.0);
    }

    public SynapseV1(Neuron neuron, double weight) {
        this.neuron = neuron;
        this.weight = weight;
    }

    @Override
    public void setNeuron(Neuron neuron) {
        this.neuron = neuron;
    }

    @Override
    public Neuron getNeuron() {
        return this.neuron;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getWeight() {
        return this.weight;
    }
}
