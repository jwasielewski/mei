package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public interface Synapse {

    void setNeuron(Neuron neuron);

    Neuron getNeuron();

    void setWeight(double weight);

    double getWeight();
}
