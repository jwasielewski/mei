package pl.jwasielewski.mei.lib.neuralnetwork;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public interface Layer {

    List<Neuron> getNeurons();

    void addInputLayer(Layer layer);

    void createNeuronsWithType(int numberOfNeurons, NeuronType type, ActivationFunction function);

    void reset();
}
