package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */


public interface ActivationFunction {

    double eval(double value);

    double derivative(double value);

}
