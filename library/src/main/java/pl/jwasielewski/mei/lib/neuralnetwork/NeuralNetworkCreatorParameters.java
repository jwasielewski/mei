package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public interface NeuralNetworkCreatorParameters {

    void setNubmerOfLayers(int numberOfNeurons);

    void setNumberOfNeuronsInLayer(int layer, int numberOfNeurons);

    int getNumberOfLayers();

    int getNumberOfNeuronsInLayer(int layer);

}
