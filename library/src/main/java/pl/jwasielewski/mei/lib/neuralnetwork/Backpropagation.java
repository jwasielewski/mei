package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public interface Backpropagation {

    void run(BackpropagationConfiguration configuration, NeuralNetwork network);

    void setBackpropagationErrorListener(BackpropagationErrorListener listener);

}
