package pl.jwasielewski.mei.lib.util;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public class KryoUtil {

    private KryoUtil() {

    }

    public static <E> void save(String path, E data) {
        Kryo kryo = new Kryo();
        try {
            Output output = new Output(new FileOutputStream(path));
            kryo.writeObject(output, data);
            output.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static <E> E load(String path, Class<E> clazz) {
        Kryo kryo = new Kryo();
        E data = null;

        try {
            Input input = new Input(new FileInputStream(path));
            data = kryo.readObject(input, clazz);
            input.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return data;
    }
}
