package pl.jwasielewski.mei.lib.ioc;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import pl.jwasielewski.mei.lib.neuralnetwork.*;
import pl.jwasielewski.mei.lib.neuralnetwork.implv1.*;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public class IoC {

    private static IoC _instance;

    private Injector injector;

    private IoC() {
        injector = Guice.createInjector(new MeiModule());
    }

    public static void createNewInstance() {
        _instance = new IoC();
    }

    public static <T> T inject(Class<T> clazz) {
        return _instance.injector.getInstance(clazz);
    }

    private class MeiModule extends AbstractModule {

        @Override
        protected void configure() {
            bind(ActivationFunction.class).to(SigmoidActivationFunction.class);
            bind(Neuron.class).to(NeuronV1.class);
            bind(Synapse.class).to(SynapseV1.class);
            bind(Layer.class).to(LayerV1.class);
            bind(NeuralNetwork.class).to(NeuralNetworkV1.class);
            bind(NeuralNetworkConfiguration.class).to(NeuralNetworkConfigurationV1.class);
            bind(NeuralNetworkCreatorParameters.class).to(DefaultNeuralNetworkParameters.class);
            bind(Backpropagation.class).to(BackpropagationV1.class);
            bind(BackpropagationConfiguration.class).to(BackpropagationConfigurationV1.class);
        }
    }
}
