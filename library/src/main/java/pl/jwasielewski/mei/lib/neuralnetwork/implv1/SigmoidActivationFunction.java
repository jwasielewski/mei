package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.ActivationFunction;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class SigmoidActivationFunction implements ActivationFunction {

    // https://en.wikipedia.org/wiki/Activation_function
    // Logistic (a.k.a Soft step)

    @Override
    public double eval(double value) {
        return 1.0 / (1.0 + Math.pow(Math.E, -value));
    }

    @Override
    public double derivative(double value) {
        return value * (1.0 - value);
    }
}
