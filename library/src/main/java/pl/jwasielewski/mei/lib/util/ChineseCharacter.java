package pl.jwasielewski.mei.lib.util;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class ChineseCharacter {

    private final int id;
    private final String character;
    private final String reading;

    public ChineseCharacter() {
        this(-1, "", "");
    }

    public ChineseCharacter(int id, String character, String reading) {
        this.id = id;
        this.character = character;
        this.reading = reading;
    }

    public int getId() {
        return id;
    }

    public String getCharacter() {
        return character;
    }

    public String getReading() {
        return reading;
    }
}
