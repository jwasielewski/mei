package pl.jwasielewski.mei.lib.neuralnetwork;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public interface Neuron {

    NeuronType getType();

    void setOutput(double output);

    double getOutput();

    double getDerivative();

    void setError(double error);

    double getError();

    void addNewSynapse(Synapse synapse);

    Synapse getSynapseForNeuron(Neuron neuron);

    List<Synapse> getSynapses();

    void reset();
}
