package pl.jwasielewski.mei.lib.neuralnetwork;

import pl.jwasielewski.mei.lib.util.ChineseCharacter;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 24/10/2015
 */

public interface NeuralNetwork {

    void setConfiguration(NeuralNetworkConfiguration configuration);

    NeuralNetworkConfiguration getConfiguration();

    void setInput(List<Double> input) throws Exception;

    List<Double> getRawOutput();

    List<ChineseCharacter> getOutput();

    int getNumberOfLayers();

    Layer getLayer(int i);

}
