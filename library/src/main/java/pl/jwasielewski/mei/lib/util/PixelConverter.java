package pl.jwasielewski.mei.lib.util;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class PixelConverter {

    private PixelConverter() {

    }

    public static List<Double> convertPixelsToList(Image image) {
        List<Double> result = new ArrayList<>();
        PixelReader pixelReader = image.getPixelReader();

        if (image.getWidth() != 24 && image.getHeight() != 24) {
            throw new IllegalArgumentException("Image has the wrong size");
        }

        for (int y = 0; y < image.getHeight(); ++y) {
            for (int x = 0; x < image.getWidth(); ++x) {
                result.add(!pixelReader.getColor(x, y).equals(Color.WHITE) ? 0.9 : 0.1);
            }
        }

        return result;
    }
}
