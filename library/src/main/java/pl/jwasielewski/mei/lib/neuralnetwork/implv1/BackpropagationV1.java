package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwasielewski.mei.lib.neuralnetwork.*;
import pl.jwasielewski.mei.lib.util.PixelConverter;
import pl.jwasielewski.mei.lib.util.PngUtil;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class BackpropagationV1 implements Backpropagation {
    private static final Logger log = LoggerFactory.getLogger(BackpropagationV1.class);

    private BackpropagationErrorListener backpropagationErrorListener;
    private Map<Synapse, Double> lastSynapseDeltaChange;

    private int outputLayerSize;

    public BackpropagationV1() {
        lastSynapseDeltaChange = new HashMap<>();
    }

    @Override
    public void run(BackpropagationConfiguration configuration, NeuralNetwork network) {
        double meanSquaredError;
        long epoch = 0;

        outputLayerSize = network.getLayer(network.getNumberOfLayers() - 1).getNeurons().size();

        List<String> trainingFiles = configuration.getAllTrainingDataFiles();
        List<HandyTrainingData> trainingInputs = new ArrayList<>();

        for (String file : trainingFiles) {
            try {
                trainingInputs.add(new HandyTrainingData(
                        PixelConverter.convertPixelsToList(PngUtil.openPng(file)),
                        getExpectedOutputFromFileName(file))
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        log.info("Backpropagation started");
        do {
            meanSquaredError = 0.0;
            lastSynapseDeltaChange.clear();
            Collections.shuffle(trainingInputs, new Random(System.nanoTime()));

            for (HandyTrainingData trainingData : trainingInputs) {
                meanSquaredError += backpropAlgorithm(configuration, network, trainingData);
            }

            meanSquaredError = meanSquaredError / (2.0 * trainingInputs.size() * outputLayerSize);

            if (backpropagationErrorListener != null) {
                backpropagationErrorListener.onErrorUpdate(meanSquaredError);
            }

            ++epoch;

            log.info(String.format("Epoch %d, mean squared error %.8f", epoch, meanSquaredError));
        } while (meanSquaredError > configuration.getErrorThreshold());
        log.info("Backpropagation ended");
    }

    public double backpropAlgorithm(BackpropagationConfiguration configuration, NeuralNetwork network,
                                    HandyTrainingData trainingData) {
        double globalError;
        List<Double> actual, expected;

        try {
            network.setInput(trainingData.input);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return 0.0;
        }

        actual = network.getRawOutput();
        expected = trainingData.output;

        // Propagate the errors backward through the network
        for (int layerIndex = network.getNumberOfLayers() - 1; layerIndex > 0; --layerIndex) {
            Layer layer = network.getLayer(layerIndex);
            int neuronPosition = 0;

            for (Neuron currentNeuron : layer.getNeurons()) {
                double localError;

                if (NeuronType.OUTPUT.equals(currentNeuron.getType())) {
                    // output
                    localError = currentNeuron.getDerivative() * (actual.get(neuronPosition) - expected.get(neuronPosition));
                } else {
                    // hidden
                    double sum = 0.0;

                    for (Neuron neuronFromNextLayer : network.getLayer(layerIndex + 1).getNeurons()) {
                        Synapse synapse = neuronFromNextLayer.getSynapseForNeuron(currentNeuron);
                        sum += synapse.getWeight() * neuronFromNextLayer.getError();
                    }

                    localError = currentNeuron.getDerivative() * sum;
                }

                currentNeuron.setError(localError);
                ++neuronPosition;
            }
        }

        // Update all weights
        for (int layerIndex = network.getNumberOfLayers() - 1; layerIndex > 0; --layerIndex) {
            Layer layer = network.getLayer(layerIndex);

            for (Neuron neuron : layer.getNeurons()) {
                for (Synapse synapse : neuron.getSynapses()) {
                    double delta = configuration.getLearningRate() * neuron.getError() * synapse.getNeuron().getOutput();

                    if (Objects.nonNull(lastSynapseDeltaChange.get(synapse))) {
                        delta += configuration.getMomentum() * lastSynapseDeltaChange.get(synapse);
                    }

                    lastSynapseDeltaChange.put(synapse, delta);
                    synapse.setWeight(synapse.getWeight() - delta);
                }
            }
        }

        actual = network.getRawOutput();
        globalError = calculateGlobalError(expected, actual);

        return globalError;
    }

    @Override
    public void setBackpropagationErrorListener(BackpropagationErrorListener listener) {
        this.backpropagationErrorListener = listener;
    }

    private double calculateGlobalError(List<Double> expected, List<Double> actual) {
        double error = 0.0;

        if (expected.size() != actual.size()) {
            throw new IllegalArgumentException("Outputs should be the same size");
        }

        for (int i = 0; i < expected.size(); ++i) {
            error += Math.pow(expected.get(i) - actual.get(i), 2);
        }

        return error;
    }

    private List<Double> getExpectedOutputFromFileName(String name) {
        List<Double> expectedOutput = new ArrayList<>();
        String file = new File(name).getName();
        // id_<rest_of_the_name>
        int id = Integer.valueOf(file.substring(0, file.indexOf("_")));

        for (int i = 0; i < outputLayerSize; ++i) {
            expectedOutput.add(0.1);
        }

        expectedOutput.set(id, 0.9);

        return expectedOutput;
    }

    public static class HandyTrainingData {
        public List<Double> input;
        public List<Double> output;

        public HandyTrainingData(List<Double> input, List<Double> output) {
            this.input = input;
            this.output = output;
        }
    }
}
