package pl.jwasielewski.mei.lib.neuralnetwork;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public interface BackpropagationConfiguration {

    void setErrorThreshold(double value);

    double getErrorThreshold();

    void setMomentum(double value);

    double getMomentum();

    void setLearingRate(double value);

    double getLearningRate();

    void setTrainingFiles(List<String> files);

    List<String> getAllTrainingDataFiles();

}
