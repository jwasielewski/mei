package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwasielewski.mei.lib.ioc.IoC;
import pl.jwasielewski.mei.lib.neuralnetwork.*;
import pl.jwasielewski.mei.lib.neuralnetwork.util.WageRandomizer;
import pl.jwasielewski.mei.lib.util.ChineseCharacter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class NeuralNetworkV1 implements NeuralNetwork {
    private static final Logger log = LoggerFactory.getLogger(NeuralNetworkV1.class);

    private static final int NUMBER_OF_RESULTS = 4;

    private final int INPUT_LAYER;
    private final int OUTPUT_LAYER;

    private List<Layer> layers;
    private NeuralNetworkConfiguration configuration;

    public NeuralNetworkV1() {
        this(0, 0);
    }

    public NeuralNetworkV1(int inputLayerId, int outputLayerId) {
        this.INPUT_LAYER = inputLayerId;
        this.OUTPUT_LAYER = outputLayerId;

        this.layers = new ArrayList<>();
    }

    public static NeuralNetwork createNeuralNetwork(NeuralNetworkCreatorParameters parameters) {
        NeuralNetworkV1 neuralNetwork = new NeuralNetworkV1(0, parameters.getNumberOfLayers() - 1);

        WageRandomizer.newInstance(parameters.getNumberOfNeuronsInLayer(0));

        for (int i = 0; i < parameters.getNumberOfLayers(); ++i) {
            Layer layer = new LayerV1();

            if (i == 0) {
                layer.createNeuronsWithType(parameters.getNumberOfNeuronsInLayer(i), NeuronType.INPUT,
                        IoC.inject(ActivationFunction.class));
            }

            if (i == parameters.getNumberOfLayers() - 1) {
                layer.createNeuronsWithType(parameters.getNumberOfNeuronsInLayer(i), NeuronType.OUTPUT,
                        IoC.inject(ActivationFunction.class));
            }

            if (i > 0 && i < parameters.getNumberOfLayers() - 1) {
                layer.createNeuronsWithType(parameters.getNumberOfNeuronsInLayer(i), NeuronType.HIDDEN,
                        IoC.inject(ActivationFunction.class));
            }

            neuralNetwork.addLayer(layer);
        }

        neuralNetwork.connectAllLayers();

        return neuralNetwork;
    }

    private void addLayer(Layer layer) {
        layers.add(layer);
    }

    private void connectAllLayers() {
        for (int i = layers.size() - 1; i > 0; --i) {
            layers.get(i).addInputLayer(layers.get(i - 1));
        }
    }

    @Override
    public void setConfiguration(NeuralNetworkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public NeuralNetworkConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public void setInput(List<Double> input) throws Exception {
        if (input.size() != layers.get(INPUT_LAYER).getNeurons().size()) {
            throw new IllegalArgumentException("Illegal input size!");
        }

        for (int i = 0; i < input.size(); ++i) {
            layers.get(INPUT_LAYER).getNeurons().get(i).setOutput(input.get(i));
        }
    }

    @Override
    public List<Double> getRawOutput() {
        List<Double> result = layers.get(OUTPUT_LAYER)
                .getNeurons()
                .stream()
                .map(Neuron::getOutput)
                .collect(Collectors.toList());

        layers.stream().forEach(Layer::reset);

        return result;
    }

    @Override
    public List<ChineseCharacter> getOutput() {
        List<Double> rawOutput = getRawOutput();
        List<ChineseCharacter> result = new ArrayList<>();
        List<Entry<Double, Integer>> outputForSort = new ArrayList<>();

        for (int i = 0; i < rawOutput.size(); ++i) {
            outputForSort.add(new Entry<>(rawOutput.get(i), i));
        }

        Collections.sort(outputForSort, (a, b) -> Double.compare(a.key, b.key));
        Collections.reverse(outputForSort);

        for (int i = 0; i < NUMBER_OF_RESULTS; ++i) {
            result.add(configuration.getCharacterForId(outputForSort.get(i).value));
        }

        return result;
    }

    @Override
    public int getNumberOfLayers() {
        return layers.size();
    }

    @Override
    public Layer getLayer(int i) {
        return layers.get(i);
    }

    private class Entry<K, V> {
        public K key;
        public V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
