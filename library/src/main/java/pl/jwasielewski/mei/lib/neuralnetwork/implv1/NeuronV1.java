package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.ActivationFunction;
import pl.jwasielewski.mei.lib.neuralnetwork.Neuron;
import pl.jwasielewski.mei.lib.neuralnetwork.NeuronType;
import pl.jwasielewski.mei.lib.neuralnetwork.Synapse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 14/11/2015
 */

public class NeuronV1 implements Neuron {

    private final NeuronType type;
    private final ActivationFunction activationFunction;

    private transient double output;
    private transient double derivative;
    private transient double error;
    private transient boolean outputHasBeenComputed;

    private List<Synapse> synapses;

    public NeuronV1() {
        this(null, null);
    }

    public NeuronV1(NeuronType type, ActivationFunction function) {
        this.type = type;
        this.activationFunction = function;
        this.synapses = new ArrayList<>();

        this.outputHasBeenComputed = false;
    }

    @Override
    public NeuronType getType() {
        return type;
    }

    @Override
    public void setOutput(double output) {
        if (NeuronType.INPUT.equals(type) || NeuronType.BIAS.equals(type)) {
            this.output = output;
            this.derivative = activationFunction.derivative(output);
        }
    }

    @Override
    public double getOutput() {
        if (NeuronType.INPUT.equals(type) || NeuronType.BIAS.equals(type)) {
            return output;
        }

        if (!outputHasBeenComputed) {
            double sum = 0.0;
            for (Synapse synapse : synapses) {
                sum += synapse.getWeight() * synapse.getNeuron().getOutput();
            }
            output = activationFunction.eval(sum);
            derivative = activationFunction.derivative(output);
            outputHasBeenComputed = true;
        }

        return output;
    }

    @Override
    public double getDerivative() {
        return this.derivative;
    }

    @Override
    public void setError(double error) {
        this.error = error;
    }

    @Override
    public double getError() {
        return this.error;
    }

    @Override
    public void addNewSynapse(Synapse synapse) {
        this.synapses.add(synapse);
    }

    @Override
    public Synapse getSynapseForNeuron(Neuron neuron) {
        for (Synapse synapse : synapses) {
            if (synapse.getNeuron() == neuron) {
                return synapse;
            }
        }

        return null;
    }

    @Override
    public List<Synapse> getSynapses() {
        return this.synapses;
    }

    @Override
    public void reset() {
        outputHasBeenComputed = false;
        error = 0;
    }

}
