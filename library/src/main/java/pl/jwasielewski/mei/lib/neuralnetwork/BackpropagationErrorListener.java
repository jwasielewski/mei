package pl.jwasielewski.mei.lib.neuralnetwork;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public interface BackpropagationErrorListener {

    void onErrorUpdate(double error);

}
