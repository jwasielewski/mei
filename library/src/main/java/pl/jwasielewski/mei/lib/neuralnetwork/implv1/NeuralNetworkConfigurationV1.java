package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.neuralnetwork.NeuralNetworkConfiguration;
import pl.jwasielewski.mei.lib.util.ChineseCharacter;

import java.util.*;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class NeuralNetworkConfigurationV1 implements NeuralNetworkConfiguration {

    private List<ChineseCharacter> characters;

    public NeuralNetworkConfigurationV1() {
        characters = new ArrayList<>();
    }

    @Override
    public void addNewChineseCharacter(ChineseCharacter character) {
        characters.add(character);
    }

    @Override
    public ChineseCharacter getCharacterForId(int id) {
        return id < characters.size() ? characters.get(id) : null;
    }


}
