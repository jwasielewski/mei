package pl.jwasielewski.mei.lib.neuralnetwork.implv1;

import pl.jwasielewski.mei.lib.ioc.IoC;
import pl.jwasielewski.mei.lib.neuralnetwork.ActivationFunction;
import pl.jwasielewski.mei.lib.neuralnetwork.Layer;
import pl.jwasielewski.mei.lib.neuralnetwork.Neuron;
import pl.jwasielewski.mei.lib.neuralnetwork.NeuronType;
import pl.jwasielewski.mei.lib.neuralnetwork.util.WageRandomizer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 25/01/2016
 */

public class LayerV1 implements Layer {

    private Neuron bias;
    private List<Neuron> neurons;
    private Layer inputLayer;

    public LayerV1() {
        this.neurons = new ArrayList<>();
    }

    @Override
    public List<Neuron> getNeurons() {
        return this.neurons;
    }

    @Override
    public void addInputLayer(Layer layer) {
        this.inputLayer = layer;

        connectNeuronsWithInputLayer();
    }

    @Override
    public void createNeuronsWithType(int numberOfNeurons, NeuronType type, ActivationFunction function) {
        for (int i = 0; i < numberOfNeurons; ++i) {
            neurons.add(new NeuronV1(type, function));
        }
    }

    private void connectNeuronsWithInputLayer() {
        for (Neuron inputLayerNeuron : inputLayer.getNeurons()) {
            for (Neuron neuron : neurons) {
                neuron.addNewSynapse(new SynapseV1(inputLayerNeuron, WageRandomizer.getInstance().getRandomWeight()));
            }
        }

        bias = new NeuronV1(NeuronType.BIAS, IoC.inject(ActivationFunction.class));
        bias.setOutput(1);

        for (Neuron neuron : neurons) {
            neuron.addNewSynapse(new SynapseV1(bias, WageRandomizer.getInstance().getRandomWeight()));
        }
    }

    @Override
    public void reset() {
        neurons.stream().forEach(Neuron::reset);
    }
}
