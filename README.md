# README #

### What is this repository for? ###

* Final uni project
* An application that recognizes chinese characters with use of neural networks
* Version 1.0

### How do I get set up? ###

Compilation and build:
gradlew clean build oneJar

Run program:
java -jar build\libs\mei-1.0.0.jar