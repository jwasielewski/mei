package pl.jwasielewski.mei;

/**
 * Created by Jedrzej Wasielewski.
 * 01/09/2015
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwasielewski.mei.lib.ioc.IoC;

public class Mei extends Application {
    private static final Logger log = LoggerFactory.getLogger(Mei.class);

    private static final String WINDOW_TITLE = "Mei";
    private static final int WINDOW_WIDTH = 800;
    private static final int WINDOW_HEIGHT = 600;
    private static final String WINDOW_ICON = "/res/icon.png";
    private static final String MAIN_WINDOW_FXML = "/pl/jwasielewski/mei/view/main_window.fxml";

    public static void main(String... args) {
        log.info("App has started");

        setupLibrary();

        launch(args);
    }

    private static void setupLibrary() {
        IoC.createNewInstance();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(MAIN_WINDOW_FXML));
        primaryStage.setTitle(WINDOW_TITLE);
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT));
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream(WINDOW_ICON)));
        primaryStage.show();
    }
}
