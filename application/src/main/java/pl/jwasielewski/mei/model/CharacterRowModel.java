package pl.jwasielewski.mei.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Jedrzej Wasielewski.
 * 02/02/2016
 */

public class CharacterRowModel {

    private final IntegerProperty id;
    private final StringProperty character;
    private final StringProperty reading;

    public CharacterRowModel() {
        this(null, null, null);
    }

    public CharacterRowModel(int id, String character, String reading) {
        this.id = new SimpleIntegerProperty(id);
        this.character = new SimpleStringProperty(character);
        this.reading = new SimpleStringProperty(reading);
    }

    public CharacterRowModel(IntegerProperty id, StringProperty character, StringProperty reading) {
        this.id = id;
        this.character = character;
        this.reading = reading;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getCharacter() {
        return character.get();
    }

    public StringProperty characterProperty() {
        return character;
    }

    public String getReading() {
        return reading.get();
    }

    public StringProperty readingProperty() {
        return reading;
    }
}
