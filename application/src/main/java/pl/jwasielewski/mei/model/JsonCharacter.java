package pl.jwasielewski.mei.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jedrzej Wasielewski.
 * 26/01/2016
 */

public class JsonCharacter {

    private int id;

    private String character;

    private String reading;

    @SerializedName("samples_number")
    private int samplesNumber;

    public JsonCharacter() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

    public int getSamplesNumber() {
        return samplesNumber;
    }

    public void setSamplesNumber(int samplesNumber) {
        this.samplesNumber = samplesNumber;
    }
}
