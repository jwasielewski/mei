package pl.jwasielewski.mei.model;

import java.util.List;

/**
 * Created by Jedrzej Wasielewski.
 * 26/01/2016
 */

public class JsonCharactersConfiguration {

    private List<JsonCharacter> characters;

    public JsonCharactersConfiguration() {

    }

    public List<JsonCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(List<JsonCharacter> characters) {
        this.characters = characters;
    }
}
