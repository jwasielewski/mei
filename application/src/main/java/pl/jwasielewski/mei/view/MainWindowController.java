package pl.jwasielewski.mei.view;

import com.google.gson.Gson;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.jwasielewski.mei.lib.components.ResizableCanvas;
import pl.jwasielewski.mei.lib.ioc.IoC;
import pl.jwasielewski.mei.lib.neuralnetwork.Backpropagation;
import pl.jwasielewski.mei.lib.neuralnetwork.BackpropagationConfiguration;
import pl.jwasielewski.mei.lib.neuralnetwork.NeuralNetwork;
import pl.jwasielewski.mei.lib.neuralnetwork.NeuralNetworkConfiguration;
import pl.jwasielewski.mei.lib.neuralnetwork.implv1.DefaultNeuralNetworkParameters;
import pl.jwasielewski.mei.lib.neuralnetwork.implv1.NeuralNetworkV1;
import pl.jwasielewski.mei.lib.util.ChineseCharacter;
import pl.jwasielewski.mei.lib.util.KryoUtil;
import pl.jwasielewski.mei.lib.util.PixelConverter;
import pl.jwasielewski.mei.lib.util.PngUtil;
import pl.jwasielewski.mei.model.JsonCharacter;
import pl.jwasielewski.mei.model.JsonCharactersConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Jedrzej Wasielewski.
 * 01/09/2015
 */

public class MainWindowController {

    private static final String WINDOW_ICON = "/res/icon.png";
    private static final String CHARACTERS_WINDOW_FXML = "/pl/jwasielewski/mei/view/characters_window.fxml";

    /****************************************************************/
    // main panel

    @FXML
    private AnchorPane canvasPane;
    @FXML
    private Button chooseNeuralNetwork;
    @FXML
    private Button showSupportedCharacters;
    @FXML
    private Button clear;
    @FXML
    private Button runDetection;
    @FXML
    private Label resultLabel;
    @FXML
    private Label result1;
    @FXML
    private Label result2;
    @FXML
    private Label result3;
    @FXML
    private Label result4;

    private double lastX, lastY;
    private NeuralNetwork choosenNeuralNetwork;
    /****************************************************************/
    // trainer

    @FXML
    private Button selectCharactersConfigurationFile;
    @FXML
    private Button selectFolderWithSamples;
    @FXML
    private Button processSamples;
    @FXML
    private Label processSamplesStatus;
    @FXML
    private TextField errorThreshold;
    @FXML
    private TextField momentum;
    @FXML
    private TextField learningRate;
    @FXML
    private Button createNeuralNetwork;
    @FXML
    private TextField neuralNetworkFileName;
    @FXML
    private Button runBackpropagation;
    @FXML
    private Label currentError;

    @FXML
    private Label iStep;
    @FXML
    private Label iiStep;
    @FXML
    private Label iiiStep;
    @FXML
    private Label ivStep;
    @FXML
    private Label waitStep;

    private JsonCharactersConfiguration jsonCharactersConfiguration;
    private File directoryWithSamples;
    private NeuralNetwork networkToTraining;

    /****************************************************************/

    @FXML
    private void initialize() {
        initializeMainPanel();
        initializeTrainerPanel();
    }

    private void initializeMainPanel() {
        lastX = lastY = -1.0;

        ResizableCanvas canvas = new ResizableCanvas();
        canvasPane.getChildren().add(canvas);
        canvas.widthProperty().bind(canvasPane.widthProperty());
        canvas.heightProperty().bind(canvasPane.heightProperty());

        final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineCap(StrokeLineCap.ROUND);
        gc.setLineJoin(StrokeLineJoin.ROUND);

        canvas.setOnMousePressed(e -> {
            lastX = e.getX();
            lastY = e.getY();
        });

        canvas.setOnMouseReleased(e -> {
            lastX = lastY = -1.0;
        });

        canvas.setOnMouseDragged(e -> {
            if (lastX != -1.0 && lastY != -1) {
                gc.setStroke(Color.BLACK);
                gc.setLineWidth(5);
                gc.strokeLine(lastX, lastY, e.getX(), e.getY());

                lastX = e.getX();
                lastY = e.getY();
            }
        });

        clear.setOnMouseClicked(e -> {
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

            gc.setFill(Color.WHITE);
            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        });

        chooseNeuralNetwork.setOnMouseClicked(e -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(".."));
            File file = chooser.showOpenDialog(chooseNeuralNetwork.getScene().getWindow());

            choosenNeuralNetwork = KryoUtil.load(file.getAbsolutePath(), NeuralNetworkV1.class);
            showSupportedCharacters.setDisable(false);
        });

        showSupportedCharacters.setOnMouseClicked(e -> {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainWindowController.class.getResource(CHARACTERS_WINDOW_FXML));
                AnchorPane page = loader.load();

                Stage stage = new Stage();
                stage.setTitle("Supported characters");
                stage.initModality(Modality.NONE);
                stage.setResizable(false);
                stage.getIcons().add(new Image(getClass().getResourceAsStream(WINDOW_ICON)));
                stage.initOwner(showSupportedCharacters.getScene().getWindow());
                Scene scene = new Scene(page);
                stage.setScene(scene);

                CharactersWindowController controller = loader.getController();
                controller.setSupportedCharacters(choosenNeuralNetwork.getConfiguration());

                stage.show();
            } catch (IOException ioex) {
                showErrorAlert(ioex.getMessage());
            }
        });

        runDetection.setOnMouseClicked(e -> {
            if (choosenNeuralNetwork != null) {
                try {
                    choosenNeuralNetwork.setInput(
                            PixelConverter.convertPixelsToList(
                                    PngUtil.scaleImage(
                                            canvas.snapshot(new SnapshotParameters(), null),
                                            (int) canvas.getWidth(),
                                            (int) canvas.getHeight()
                                    )
                            )
                    );

                    List<ChineseCharacter> result = choosenNeuralNetwork.getOutput();

                    resultLabel.setVisible(true);

                    result1.setText(result.get(0).getCharacter());
                    result2.setText(result.get(1).getCharacter());
                    result3.setText(result.get(2).getCharacter());
                    result4.setText(result.get(3).getCharacter());

                } catch (Exception e1) {
                    System.out.println(e1.getMessage());
                    e1.printStackTrace();
                }
            }
        });
    }

    private void initializeTrainerPanel() {
        selectCharactersConfigurationFile.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select characters configuration file");
            fileChooser.setInitialDirectory(new File("."));
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JSON", "*.json")
            );

            File choosenFile = fileChooser.showOpenDialog(selectCharactersConfigurationFile.getScene().getWindow());

            Gson gson = new Gson();
            try {
                jsonCharactersConfiguration = gson.fromJson(
                        new InputStreamReader(new FileInputStream(choosenFile), "UTF-8"),
                        JsonCharactersConfiguration.class
                );

                testCharactersConfigurationFile(jsonCharactersConfiguration);

                selectCharactersConfigurationFile.setDisable(true);
                iStep.setVisible(false);
                enableSamplesStep();
            } catch (Exception ex) {
                ex.printStackTrace();

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Something gone wrong!");
                alert.setHeaderText(null);
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
            }
        });
    }

    private void testCharactersConfigurationFile(JsonCharactersConfiguration configuration) throws Exception {
        if (Objects.nonNull(configuration) && Objects.nonNull(configuration.getCharacters())
                && configuration.getCharacters().size() > 0) {
            configuration.getCharacters().get(0).getCharacter();
        } else {
            throw new Exception("Invalid file");
        }
    }

    private void enableSamplesStep() {
        selectFolderWithSamples.setDisable(false);
        iiStep.setVisible(true);

        selectFolderWithSamples.setOnMouseClicked(e -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Choose directory with");
            directoryChooser.setInitialDirectory(new File("."));

            directoryWithSamples = directoryChooser.showDialog(selectFolderWithSamples.getScene().getWindow());
            if (Objects.isNull(directoryWithSamples) || Objects.isNull(directoryWithSamples.listFiles())
                    || directoryWithSamples.listFiles().length == 0) {
                showErrorAlert("Invalid directory");
            } else {
                processSamples.setDisable(false);
            }
        });

        processSamples.setOnMouseClicked(e -> {
            File[] files = directoryWithSamples.listFiles(pathname -> pathname.toString().endsWith(".png"));
            final int count = files.length;
            int i = 0;
            for (File file : files) {
                try {
                    Image image = PngUtil.openPng(file.getAbsolutePath());
                    image = PngUtil.scaleImage(image, (int) image.getWidth(), (int) image.getHeight());
                    PngUtil.savePng(file.getAbsolutePath(), image);

                    processSamplesStatus.setText(String.format("%d/%d", ++i, count));
                } catch (IOException e1) {
                    showErrorAlert(e1.getMessage());
                    processSamplesStatus.setText("0/0");
                    return;
                }

                selectFolderWithSamples.setDisable(true);
                processSamples.setDisable(true);
                iiStep.setVisible(false);
                enableNeuralNetworkDetails();
            }
        });
    }

    private void enableNeuralNetworkDetails() {
        createNeuralNetwork.setDisable(false);
        neuralNetworkFileName.setDisable(false);
        iiiStep.setVisible(true);

        createNeuralNetwork.setOnMouseClicked(e -> {
            if (neuralNetworkFileName.getText().length() > 0 && !neuralNetworkFileName.getText().contains(" ")) {
                networkToTraining = NeuralNetworkV1.createNeuralNetwork(new DefaultNeuralNetworkParameters());

                NeuralNetworkConfiguration neuralNetworkConfiguration = IoC.inject(NeuralNetworkConfiguration.class);
                for (JsonCharacter character : jsonCharactersConfiguration.getCharacters()) {
                    neuralNetworkConfiguration.addNewChineseCharacter(new ChineseCharacter(
                            character.getId(),
                            character.getCharacter(),
                            character.getReading()
                    ));
                }
                networkToTraining.setConfiguration(neuralNetworkConfiguration);


                KryoUtil.save(neuralNetworkFileName.getText(), networkToTraining);
            } else {
                showErrorAlert("Insert valid neural network filename (without spaces)!");
                return;
            }

            createNeuralNetwork.setDisable(true);
            neuralNetworkFileName.setDisable(true);
            iiiStep.setVisible(false);
            enableBackpropagation();
        });
    }

    private void enableBackpropagation() {
        errorThreshold.setDisable(false);
        momentum.setDisable(false);
        learningRate.setDisable(false);
        runBackpropagation.setDisable(false);
        ivStep.setVisible(true);

        runBackpropagation.setOnMouseClicked(e -> {
            if (!checkIfBackpropagationDetailsAreValid()) {
                showErrorAlert("Error threshold, momentum or learning rate is not valid decimal number!");
                return;
            }

            errorThreshold.setDisable(true);
            momentum.setDisable(true);
            learningRate.setDisable(true);
            runBackpropagation.setDisable(true);
            ivStep.setVisible(false);

            waitStep.setVisible(true);

            Backpropagation backpropagation = IoC.inject(Backpropagation.class);

            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    backpropagation.setBackpropagationErrorListener(error -> updateMessage(String.format("%.6f", error)));

                    BackpropagationConfiguration backpropagationConfiguration = IoC.inject(BackpropagationConfiguration.class);

                    List<String> files = new ArrayList<>();
                    for (File file : directoryWithSamples.listFiles(pathname -> pathname.toString().endsWith(".png"))) {
                        files.add(file.getAbsolutePath());
                    }
                    backpropagationConfiguration.setTrainingFiles(files);

                    backpropagationConfiguration.setErrorThreshold(Double.valueOf(errorThreshold.getText()));
                    backpropagationConfiguration.setMomentum(Double.valueOf(momentum.getText()));
                    backpropagationConfiguration.setLearingRate(Double.valueOf(learningRate.getText()));

                    backpropagation.run(backpropagationConfiguration, networkToTraining);

                    return null;
                }
            };

            task.setOnSucceeded(a -> {
                KryoUtil.save(neuralNetworkFileName.getText(), networkToTraining);

                currentError.textProperty().unbind();
                currentError.setText("Done");

                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Done");
                alert.setHeaderText(null);
                alert.showAndWait();
            });

            currentError.textProperty().bind(task.messageProperty());

            Thread thread = new Thread(task);
            thread.setDaemon(true);
            thread.start();
        });
    }

    private boolean checkIfBackpropagationDetailsAreValid() {
        try {
            Double.valueOf(errorThreshold.getText());
            Double.valueOf(momentum.getText());
            Double.valueOf(learningRate.getText());
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    private void showErrorAlert(String messsage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Something gone wrong!");
        alert.setHeaderText(null);
        alert.setContentText(messsage);
        alert.showAndWait();
    }

}
