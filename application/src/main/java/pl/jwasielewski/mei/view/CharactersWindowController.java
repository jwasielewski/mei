package pl.jwasielewski.mei.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pl.jwasielewski.mei.lib.neuralnetwork.NeuralNetworkConfiguration;
import pl.jwasielewski.mei.lib.util.ChineseCharacter;
import pl.jwasielewski.mei.model.CharacterRowModel;

import java.util.Objects;

/**
 * Created by Jedrzej Wasielewski.
 * 02/02/2016
 */

public class CharactersWindowController {

    @FXML
    private TableView<CharacterRowModel> tableView;
    @FXML
    private TableColumn<CharacterRowModel, Number> idColumn;
    @FXML
    private TableColumn<CharacterRowModel, String> characterColumn;
    @FXML
    private TableColumn<CharacterRowModel, String> readingColumn;

    private ObservableList<CharacterRowModel> characterRowModels = FXCollections.observableArrayList();

    @FXML
    public void initialize() {

        idColumn.setCellValueFactory(data -> data.getValue().idProperty());
        characterColumn.setCellValueFactory(data -> data.getValue().characterProperty());
        readingColumn.setCellValueFactory(data -> data.getValue().readingProperty());

        tableView.setItems(characterRowModels);

    }

    public void setSupportedCharacters(NeuralNetworkConfiguration configuration) {
        for (int i = 0; i < Integer.MAX_VALUE; ++i) {
            if (Objects.nonNull(configuration.getCharacterForId(i))) {
                ChineseCharacter character = configuration.getCharacterForId(i);
                characterRowModels.add(new CharacterRowModel(
                        character.getId(),
                        character.getCharacter(),
                        character.getReading()
                ));
            } else {
                break;
            }
        }
    }
}
